#!/usr/bin/perl


use LWP::UserAgent;
use Sys::Syslog qw(:DEFAULT setlogsock);
use POSIX;

sub fatal {
    my $msg = shift;
    syslog('crit', $msg);
    print("webcam-update.pl: ". $msg ."\n");
   #exit(1);
}

sub info {
    my $msg = shift;
    syslog('info', $msg);
}

sub mail {
    my $msg = shift;
   #my @m = ('9426778@smsmail.lmt.lv');
    my @m = ();
    
    foreach my $phone (@m) {
        if(!open(SMS, "| mail $phone")) {
	    syslog('crit', "open() pipe to mail failed: $!");
	    return;
	}
	print SMS $msg;
	close(SMS);
    }
}

    my $url         = 'http://cam.hosting.lv/cam0.jpg';
    my $base        = '/var/db/webcam/';
    my $file        = $base .'cam.jpg';
    my $file_new    = $base .'cam-new.jpg';
    my $flag_file   = $base .'client/present';
    my $drop_update = 30;

    my $force_update = 30;
    my $pic_ok = 5;

    my $a = new LWP::UserAgent;
    $a->agent('WebCam Update '. $a->agent());

    setlogsock('unix');
    openlog('webcam-update.pl', 'pid', 'webcam-update');
        
    {
#    my $uid = getpwnam("nobody");
#    my $gid = getgrnam("nobody");
#    
#    if(!$uid || !$gid) {
#        fatal("get{pw,gr}nam(\"nobody\") failed: $!");
#    }
#    if(setgid($gid) != $gid || setuid($uid) != $uid) {
#        fatal("set{u,g}id(\"nobody\") failed: $!");
#    }

    chdir('/');
    defined(my $pid = fork()) || fatal("fork() failed: $!");
    exit() if $pid;
    setsid();
    close(STDIN);
    close(STDOUT);
    close(STDERR);
    }
    

    my $started = time();
    while(1) {
	$mtime = (stat($flag_file))[9];

	# lazy update if no client present
	if($force_update != 30) {
	    if($mtime + $drop_update < time()) {
		++$force_update;
		sleep(2);
		next;
	    }
	}

	$force_update = 0;

	my $r = new HTTP::Request('GET' => $url);
	my $z = $a->request($r);

	if(!$z->is_success()) {
	    if($pic_ok != -1 && --$pic_ok == -1) {
	        syslog('crit', "request failed: GET \"$url\": ". $z->status_line());
		mail('webcam update problem');
	    }
	    sleep(60);
	} else {
	    if($pic_ok == -1) { 
	        info('connection ok');
		mail('webcam connection ok');
	    }
	    $pic_ok = 5;
	    open(PIC, "> $file_new") || fatal("open(\"$file_new\") failed: $!");
	    print PIC $z->content()  || fatal("print() failed: $!");
	    close(PIC);
	    rename($file_new, $file) || fatal("rename() failed: $!");
	    #print "updated\n";
	    sleep(2);
	}
    }
