#include <stdlib.h>
#include <string.h>

#include "deinterlace.h"

#define FRAME_ONLY 0
#define FIELD_ONLY 1
#define FRAME_AND_FIELD 2

#define R_MASK  (0x00ff0000)
#define G_MASK  (0x0000ff00)
#define B_MASK  (0x000000ff)
#define R_SHIFT         (16)
#define G_SHIFT          (8)
#define B_SHIFT          (0)

void bgr2argb(char *in, unsigned int *out, int width, int height)
{
    int run;
    int size = width*height;

    for (run = 0; run < size; run++) {
        *out = (((((Pixel32) *(in+2)) & 0xff) << R_SHIFT) |
                ((((Pixel32) *(in+1)) & 0xff) << G_SHIFT) |
                ((((Pixel32) *(in+0)) & 0xff)))      & 0x00ffffff;
        out++;
        in += 3;
    }
}

void argb2bgr(unsigned int *in, char *out, int width, int height)
{
    int run;
    int size = width*height;

    for (run = 0; run < size; run++) {
        *(out+2) = ((*in & R_MASK) >> R_SHIFT)&0xff;
        *(out+1) = ((*in & G_MASK) >> G_SHIFT)&0xff;
        *(out+0) = (*in) & B_MASK;
        in++;
        out += 3;
    }
}

Deinterlacer::Deinterlacer(int x, int y)
{
    first_frame = 1;
    width = x;
    height = y;
    convertFrameIn = (Pixel32 *) malloc (width * height * sizeof(Pixel32));
    memset(convertFrameIn, 0, width * height * sizeof(Pixel32));
    convertFrameOut = (Pixel32 *) malloc (width * height * sizeof(Pixel32));
    memset(convertFrameOut, 0, width * height * sizeof(Pixel32));
    if(this->diffmode == FRAME_ONLY || this->diffmode == FRAME_AND_FIELD) {
        prevFrame = (int *) malloc (width*height*sizeof(int));
        memset(prevFrame, 0, width*height*sizeof(int));
    }
    if(fieldShift || (inswap && !outswap) || (!inswap && outswap))
        saveFrame = (int *) malloc (width*height*sizeof(int));
    if(!noMotion) {
        moving = (unsigned char *) malloc(sizeof(unsigned char)*width*height);
        memset(moving, 0, width*height*sizeof(unsigned char));
    }
    if(highq)
        fmoving = (unsigned char *) malloc(sizeof(unsigned char)*width*height);
}
/*
Deinterlacer::~Deinterlacer()
{
    if (this->diffmode == FRAME_ONLY || this->diffmode == FRAME_AND_FIELD)
    {
        if (this->prevFrame)
            free(this->prevFrame);
        this->prevFrame = NULL;
    }

    if (this->fieldShift ||
        (this->inswap && !this->outswap) || (!this->inswap && this->outswap))
    {
        if (this->saveFrame)
            free(this->saveFrame);
        this->saveFrame = NULL;
    }

    if (!this->noMotion)
    {
        if (this->moving)
            free(this->moving);
        this->moving = NULL;
    }

    if (this->highq)
    {
        if (this->fmoving)
            free(this->fmoving);
        this->fmoving = NULL;
    }

    if (this->convertFrameIn) {
        free (this->convertFrameIn);
        this->convertFrameIn = NULL;
    }

    if (this->convertFrameOut) {
        free (this->convertFrameOut);
        this->convertFrameOut = NULL;
    }
}
*/
void Deinterlacer::deinterlace()
{
    const int        srcpitch = width*sizeof(Pixel32);
    const int        srcpitchtimes2 = 2 * srcpitch;
    const int        dstpitch = width*sizeof(Pixel32);
    const int        dstpitchtimes2 = 2 * dstpitch;

    const PixDim     w = width;
    const int        wminus1 = w - 1;
    const int        wtimes2 = w * 2;
    const int        wtimes4 = w * 4;

    const PixDim     h = height;
    const int        hminus1 = h - 1;
    const int        hminus3 = h - 3;
    const int        hover2 = h / 2;

    Pixel32         *src, *dst, *srcminus, *srcplus, *srcminusminus, *srcplusplus;
    unsigned char   *moving, *movingminus, *movingplus;
    unsigned char   *fmoving;
    int             *saved, *sv;
    Pixel32         *src1, *src2, *s1, *s2;
    Pixel32         *dst1, *dst2, *d1, *d2;
    int             *prev;
    int              scenechange;
    long             count;
    int              x, y;
    long             prevValue, nextValue, luma, lumap, luman;
    Pixel32          p0, p1, p2;
    long             r, g, b, rp, gp, bp, rn, gn, bn, T;
    long             rpp, gpp, bpp, rnn, gnn, bnn, R, G, B;
    unsigned char    frMotion, fiMotion;
    int              copyback;

    Pixel32 * dst_buf;
    Pixel32 * src_buf;

    src1 = NULL;
    dst1 = NULL;
    src2 = NULL;
    dst2 = NULL;
    srcminusminus = NULL;
    srcplusplus = NULL;
    luma = 0;
    saved = NULL;

    src_buf = convertFrameIn;
    dst_buf = convertFrameOut;

    /* If we are performing Advanced Processing... */
    if (this->inswap || this->outswap || this->fieldShift)
    {
        /* Advanced Processing is used typically to clean up PAL video
           which has erroneously been digitized with the field phase off by
           one field. The result is that the frames look interlaced,
           but really if we can phase shift by one field, we'll get back
           the original progressive frames. Also supported are field swaps
           before and/or after the phase shift to accommodate different
           capture cards and telecining methods, as explained in the
           help file. Finally, the user can optionally disable full
           motion processing after this processing. */
        copyback = 1;
        if (!this->fieldShift)
        {
            /* No phase shift enabled, but we have swap(s) enabled. */
            if (this->inswap && this->outswap)
            {
                if (this->noMotion)
                {
                    /* Swapping twice is a null operation. */
                    src1 = src_buf;
                    dst1 = dst_buf;
                    for (y = 0; y < h; y++)
                    {
                        memcpy(dst1, src1, wtimes4);
                        src1 = (Pixel *)((char *)src1 + srcpitch);
                        dst1 = (Pixel *)((char *)dst1 + dstpitch);
                    }
                    goto filter_done;
                }
                else
                {
                    copyback = 0;
                }
            }
            else
            {
                /* Swap fields. */
                src1 = (Pixel32 *)((char *)src_buf + srcpitch);
                saved = this->saveFrame + w;
                for (y = 0; y < hover2; y++)
                {
                    memcpy(saved, src1, wtimes4);
                    src1 = (Pixel *)((char *)src1 + srcpitchtimes2);
                    saved += wtimes2;
                }
                src1 = src_buf;
                dst1 = (Pixel32 *)((char *)dst_buf+ dstpitch);
                for (y = 0; y < hover2; y++)
                {
                    memcpy(dst1, src1, wtimes4);
                    src1 = (Pixel *)((char *)src1 + srcpitchtimes2);
                    dst1 = (Pixel *)((char *)dst1 + dstpitchtimes2);
                }
                dst1 = dst_buf;
                saved = this->saveFrame + w;
                for (y = 0; y < hover2; y++)
                {
                    memcpy(dst1, saved, wtimes4);
                    dst1 = (Pixel *)((char *)dst1 + dstpitchtimes2);
                    saved += wtimes2;
                }
            }
        }
        /* If we reach here, then phase shift has been enabled. */
        else
        {
            switch (this->inswap | (this->outswap << 1))
            {
            case 0:
                /* No inswap, no outswap. */
                src1 = src_buf;
                src2 = (Pixel32 *)((char *)src1 + srcpitch);
                dst1 = (Pixel32 *)((char *)dst_buf+ dstpitch);
                dst2 = dst_buf;
                saved = this->saveFrame + w;
                break;
            case 1:
                /* Inswap, no outswap. */
                src1 = (Pixel32 *)((char *)src_buf + srcpitch);
                src2 = src_buf;
                dst1 = (Pixel32 *)((char *)dst_buf+ dstpitch);
                dst2 = dst_buf;
                saved = this->saveFrame;
                break;
            case 2:
                /* No inswap, outswap. */
                src1 = src_buf;
                src2 = (Pixel32 *)((char *)src_buf + srcpitch);
                dst1 = dst_buf;
                dst2 = (Pixel32 *)((char *)dst_buf + dstpitch);
                saved = this->saveFrame + w;
                break;
            case 3:
                /* Inswap, outswap. */
                src1 = (Pixel32 *)((char *)src_buf + srcpitch);
                src2 = src_buf;
                dst1 = dst_buf;
                dst2 = (Pixel32 *)((char *)dst_buf + dstpitch);
                saved = this->saveFrame;
                break;
            }

            s1 = src1;
            d1 = dst1;
            for (y = 0; y < hover2; y++)
            {
                memcpy(d1, s1, wtimes4);
                s1 = (Pixel *)((char *)s1 + srcpitchtimes2);
                d1 = (Pixel *)((char *)d1 + dstpitchtimes2);
            }

            /* If this is not the first frame, copy the buffered field
               of the last frame to the output. This creates a correct progressive
               output frame. If this is the first frame, a buffered field is not
               available, so interpolate the field from the current field. */
            if (first_frame)
            {
                s1 = src1;
                d2 = dst2;
                for (y = 0; y < hover2; y++)
                {
                    memcpy(d2, s1, wtimes4);
                    s1 = (Pixel *)((char *)s1 + srcpitchtimes2);
                    d2 = (Pixel *)((char *)d2 + dstpitchtimes2);
                }
            }
            else
            {
                d2 = dst2;
                sv = saved;
                for (y = 0; y < hover2; y++)
                {
                    memcpy(d2, sv, wtimes4);
                    sv += wtimes2;
                    d2 = (Pixel *)((char *)d2 + dstpitchtimes2);
                }
            }
            /* Finally, save the unused field of the current frame in the buffer.
               It will be used to create the next frame. */
            s2 = src2;
            sv = saved;
            for (y = 0; y < hover2; y++)
            {
                memcpy(sv, s2, wtimes4);
                sv += wtimes2;
                s2 = (Pixel *)((char *)s2 + srcpitchtimes2);
            }
        }
        if (this->noMotion) goto filter_done;

        if (copyback)
        {
            /* We're going to do motion processing also, so copy
               the result back into the src bitmap. */
            src1 = dst_buf;
            dst1 = src_buf;
            for (y = 0; y < h; y++)
            {
                memcpy(dst1, src1, wtimes4);
                src1 = (Pixel *)((char *)src1 + srcpitch);
                dst1 = (Pixel *)((char *)dst1 + dstpitch);
            }
        }
    }
    else if (this->noMotion)
    {
        /* Well, I suppose somebody might select no advanced processing options
           but tick disable motion processing. This covers that. */
        src1 = src_buf;
        dst1 = dst_buf;
        for (y = 0; y < h; y++)
        {
            memcpy(dst1, src1, srcpitch);
            src1 = (Pixel *)((char *)src1 + srcpitch);
            dst1 = (Pixel *)((char *)dst1 + dstpitch);
        }
        goto filter_done;
    }

    /* End advanced processing mode code. Now do full motion-adaptive deinterlacing. */

    /* Not much deinterlacing to do if there aren't at least 2 lines. */
    if (h < 2) goto filter_done;

    count = 0;
    if (this->diffmode == FRAME_ONLY || this->diffmode == FRAME_AND_FIELD)
    {
        /* Skip first and last lines, they'll get a free ride. */
        src = (Pixel *)((char *)src_buf + srcpitch);
        srcminus = (Pixel *)((char *)src - srcpitch);
        prev = this->prevFrame + w;
        moving = this->moving + w;
        for (y = 1; y < hminus1; y++)
        {
            x = 0;
            do
            {
                // First check frame motion.
                // Set the moving flag if the diff exceeds the configured
                // threshold.
                moving[x] = 0;
                frMotion = 0;
                prevValue = *prev;
                if (!this->colordiff)
                {
                    r = (src[x] >> 16) & 0xff;
                    g = (src[x] >> 8) & 0xff;
                    b = src[x] & 0xff;
                    luma = (76 * r + 30 * b + 150 * g) >> 8;
                    if (abs(luma - prevValue) > this->threshold) frMotion = 1;
                }
                else
                {
                    b = src[x] & 0xff;
                    bp = prevValue & 0xff;
                    if (abs(b - bp) > this->threshold) frMotion = 1;
                    else
                    {
                        r = (src[x] >>16) & 0xff;
                        rp = (prevValue >> 16) & 0xff;
                        if (abs(r - rp) > this->threshold) frMotion = 1;
                        else
                        {
                            g = (src[x] >> 8) & 0xff;
                            gp = (prevValue >> 8) & 0xff;
                            if (abs(g - gp) > this->threshold) frMotion = 1;
                        }
                    }
                }

                // Now check field motion if applicable.
                if (this->diffmode == FRAME_ONLY) moving[x] = frMotion;
                else
                {
                    fiMotion = 0;
                    if (y & 1)
                        prevValue = srcminus[x];
                    else
                        prevValue = *(prev + w);
                    if (!this->colordiff)
                    {
                        r = (src[x] >> 16) & 0xff;
                        g = (src[x] >> 8) & 0xff;
                        b = src[x] & 0xff;
                        luma = (76 * r + 30 * b + 150 * g) >> 8;
                        if (abs(luma - prevValue) > this->threshold) fiMotion = 1;
                    }
                    else
                    {
                        b = src[x] & 0xff;
                        bp = prevValue & 0xff;
                        if (abs(b - bp) > this->threshold) fiMotion = 1;
                        else
                        {
                            r = (src[x] >> 16) & 0xff;
                            rp = (prevValue >> 16) & 0xff;
                            if (abs(r - rp) > this->threshold) fiMotion = 1;
                            else
                            {
                                g = (src[x] >> 8) & 0xff;
                                gp = (prevValue >> 8) & 0xff;
                                if (abs(g - gp) > this->threshold) fiMotion = 1;
                            }
                        }
                    }
                    moving[x] = (fiMotion && frMotion);
                }
                if (!this->colordiff)
                    *prev++ = luma;
                else
                    *prev++ = src[x];
                /* Keep a count of the number of moving pixels for the
                   scene change detection. */
                if (moving[x]) count++;
            } while(++x < w);
            src = (Pixel *)((char *)src + srcpitch);
            srcminus = (Pixel *)((char *)srcminus + srcpitch);
            moving += w;
        }

        /* Determine whether a scene change has occurred. */
        if ((100L * count) / (h * w) >= this->scenethreshold) scenechange = 1;
        else scenechange = 0;

        /* Perform a denoising of the motion map if enabled. */
        if (!scenechange && this->highq)
        {
            int xlo, xhi, ylo, yhi;
            int u, v;
            int N = 5;
            int Nover2 = N/2;
            int sum;
            unsigned char *m;

            // Erode.
            fmoving = this->fmoving;
            for (y = 0; y < h; y++)
            {
                for (x = 0; x < w; x++)
                {
                    if (!((this->moving + y * w)[x]))
                    {
                        fmoving[x] = 0;
                        continue;
                    }
                    xlo = x - Nover2; if (xlo < 0) xlo = 0;
                    xhi = x + Nover2; if (xhi >= w) xhi = wminus1;
                    ylo = y - Nover2; if (ylo < 0) ylo = 0;
                    yhi = y + Nover2; if (yhi >= h) yhi = hminus1;
                    m = this->moving + ylo * w;
                    sum = 0;
                    for (u = ylo; u <= yhi; u++)
                    {
                        for (v = xlo; v <= xhi; v++)
                        {
                            sum += m[v];
                        }
                        m += w;
                    }
                    if (sum > 9)
                        fmoving[x] = 1;
                    else
                        fmoving[x] = 0;
                }
                fmoving += w;
            }
            // Dilate.
            N = 5;
            Nover2 = N/2;
            moving = this->moving;
            for (y = 0; y < h; y++)
            {
                for (x = 0; x < w; x++)
                {
                    if (!((this->fmoving + y * w)[x]))
                    {
                        moving[x] = 0;
                        continue;
                    }
                    xlo = x - Nover2; if (xlo < 0) xlo = 0;
                    xhi = x + Nover2; if (xhi >= w) xhi = wminus1;
                    ylo = y - Nover2; if (ylo < 0) ylo = 0;
                    yhi = y + Nover2; if (yhi >= h) yhi = hminus1;
                    m = this->moving + ylo * w;
                    for (u = ylo; u <= yhi; u++)
                    {
                        for (v = xlo; v <= xhi; v++)
                        {
                            m[v] = 1;
                        }
                        m += w;
                    }
                }
                moving += w;
            }
        }
    }
    else
    {
        /* Field differencing only mode. */
        T = this->threshold * this->threshold;
        src = (Pixel *)((char *)src_buf + srcpitch);
        srcminus = (Pixel *)((char *)src - srcpitch);
        srcplus = (Pixel *)((char *)src + srcpitch);
        moving = this->moving + w;
        for (y = 1; y < hminus1; y++)
        {
            x = 0;
            do
            {
                // Set the moving flag if the diff exceeds the configured
                // threshold.
                moving[x] = 0;
                if (y & 1)
                {
                    // Now check field motion.
                    fiMotion = 0;
                    nextValue = srcplus[x];
                    prevValue = srcminus[x];
                    if (!this->colordiff)
                    {
                        r = (src[x] >> 16) & 0xff;
                        rp = (prevValue >> 16) & 0xff;
                        rn = (nextValue >> 16) & 0xff;
                        g = (src[x] >> 8) & 0xff;
                        gp = (prevValue >> 8) & 0xff;
                        gn = (nextValue >> 8) & 0xff;
                        b = src[x] & 0xff;
                        bp = prevValue & 0xff;
                        bn = nextValue & 0xff;
                        luma = (76 * r + 30 * b + 150 * g) >> 8;
                        lumap = (76 * rp + 30 * bp + 150 * gp) >> 8;
                        luman = (76 * rn + 30 * bn + 150 * gn) >> 8;
                        if ((lumap - luma) * (luman - luma) > T)
                            moving[x] = 1;
                    }
                    else
                    {
                        b = src[x] & 0xff;
                        bp = prevValue & 0xff;
                        bn = nextValue & 0xff;
                        if ((bp - b) * (bn - b) > T) moving[x] = 1;
                        else
                        {
                            r = (src[x] >> 16) & 0xff;
                            rp = (prevValue >> 16) & 0xff;
                            rn = (nextValue >> 16) & 0xff;
                            if ((rp - r) * (rn - r) > T) moving[x] = 1;
                            else
                            {
                                g = (src[x] >> 8) & 0xff;
                                gp = (prevValue >> 8) & 0xff;
                                gn = (nextValue >> 8) & 0xff;
                                if ((gp - g) * (gn - g) > T) moving[x] = 1;
                            }
                        }
                    }
                }
                /* Keep a count of the number of moving pixels for the
                   scene change detection. */
                if (moving[x]) count++;
            } while(++x < w);
            src = (Pixel *)((char *)src + srcpitch);
            srcminus = (Pixel *)((char *)srcminus + srcpitch);
            srcplus = (Pixel *)((char *)srcplus + srcpitch);
            moving += w;
        }

        /* Determine whether a scene change has occurred. */
        if ((100L * count) / (h * w) >= this->scenethreshold) scenechange = 1;
        else scenechange = 0;

        /* Perform a denoising of the motion map if enabled. */
        if (!scenechange && this->highq)
        {
            int xlo, xhi, ylo, yhi;
            int u, v;
            int N = 5;
            int Nover2 = N/2;
            int sum;
            unsigned char *m;

            // Erode.
            fmoving = this->fmoving;
            for (y = 0; y < h; y++)
            {
                for (x = 0; x < w; x++)
                {
                    if (!((this->moving + y * w)[x]))
                    {
                        fmoving[x] = 0;
                        continue;
                    }
                    xlo = x - Nover2; if (xlo < 0) xlo = 0;
                    xhi = x + Nover2; if (xhi >= w) xhi = wminus1;
                    ylo = y - Nover2; if (ylo < 0) ylo = 0;
                    yhi = y + Nover2; if (yhi >= h) yhi = hminus1;
                    m = this->moving + ylo * w;
                    sum = 0;
                    for (u = ylo; u <= yhi; u++)
                    {
                        for (v = xlo; v <= xhi; v++)
                        {
                            sum += m[v];
                        }
                        m += w;
                    }
                    if (sum > 9)
                        fmoving[x] = 1;
                    else
                        fmoving[x] = 0;
                }
                fmoving += w;
            }

            // Dilate.
            N = 5;
            Nover2 = N/2;
            moving = this->moving;
            for (y = 0; y < h; y++)
            {
                for (x = 0; x < w; x++)
                {
                    if (!((this->fmoving + y * w)[x]))
                    {
                        moving[x] = 0;
                        continue;
                    }
                    xlo = x - Nover2; if (xlo < 0) xlo = 0;
                    xhi = x + Nover2; if (xhi >= w) xhi = wminus1;
                    ylo = y - Nover2; if (ylo < 0) ylo = 0;
                    yhi = y + Nover2; if (yhi >= h) yhi = hminus1;
                    m = this->moving + ylo * w;
                    for (u = ylo; u <= yhi; u++)
                    {
                        for (v = xlo; v <= xhi; v++)
                        {
                            m[v] = 1;
                        }
                        m += w;
                    }
                }
                moving += w;
            }
        }
    }

    // Render.
    // The first line gets a free ride.
    src = src_buf;
    dst = dst_buf;
    memcpy(dst, src, wtimes4);
    src = (Pixel *)((char *)src_buf + srcpitch);
    srcminus = (Pixel *)((char *)src - srcpitch);
    srcplus = (Pixel *)((char *)src + srcpitch);
    if (cubic)
    {
        srcminusminus = (Pixel *)((char *)src - 3 * srcpitch);
        srcplusplus = (Pixel *)((char *)src + 3 * srcpitch);
    }
    dst = (Pixel *)((char *)dst_buf + dstpitch);
    moving = this->moving + w;
    movingminus = moving - w;
    movingplus = moving + w;
    for (y = 1; y < hminus1; y++)
    {
        if (this->motionOnly)
        {
            if (this->Blend)
            {
                x = 0;
                do {
                    if (!(movingminus[x] | moving[x] | movingplus[x]) && !scenechange)
                        dst[x] = 0x7f7f7f;
                    else
                    {
                        /* Blend fields. */
                        p0 = src[x];
                        p0 &= 0x00fefefe;

                        p1 = srcminus[x];
                        p1 &= 0x00fcfcfc;

                        p2 = srcplus[x];
                        p2 &= 0x00fcfcfc;

                        dst[x] = (p0>>1) + (p1>>2) + (p2>>2);
                    }
                } while(++x < w);
            }
            else
            {
                x = 0;
                do {
                    if (!(movingminus[x] | moving[x] | movingplus[x]) && !scenechange)
                        dst[x] = 0x7f7f7f;
                    else if (y & 1)
                    {
                        if (cubic && (y > 2) && (y < hminus3))
                        {
                            rpp = (srcminusminus[x] >> 16) & 0xff;
                            rp =  (srcminus[x] >> 16) & 0xff;
                            rn =  (srcplus[x] >> 16) & 0xff;
                            rnn = (srcplusplus[x] >>16) & 0xff;
                            gpp = (srcminusminus[x] >> 8) & 0xff;
                            gp =  (srcminus[x] >> 8) & 0xff;
                            gn =  (srcplus[x] >>8) & 0xff;
                            gnn = (srcplusplus[x] >> 8) & 0xff;
                            bpp = (srcminusminus[x]) & 0xff;
                            bp =  (srcminus[x]) & 0xff;
                            bn =  (srcplus[x]) & 0xff;
                            bnn = (srcplusplus[x]) & 0xff;
                            R = (5 * (rp + rn) - (rpp + rnn)) >> 3;
                            if (R > 255) R = 255;
                            else if (R < 0) R = 0;
                            G = (5 * (gp + gn) - (gpp + gnn)) >> 3;
                            if (G > 255) G = 255;
                            else if (G < 0) G = 0;
                            B = (5 * (bp + bn) - (bpp + bnn)) >> 3;
                            if (B > 255) B = 255;
                            else if (B < 0) B = 0;
                            dst[x] = (R << 16) | (G << 8) | B;
                        }
                        else
                        {
                            p1 = srcminus[x];
                            p1 &= 0x00fefefe;

                            p2 = srcplus[x];
                            p2 &= 0x00fefefe;
                            dst[x] = (p1>>1) + (p2>>1);
                        }
                    }
                    else
                        dst[x] = src[x];
                } while(++x < w);
            }
        }
        else  /* Not motion only */
        {
            if (this->Blend)
            {
                x = 0;
                do {
                    if (!(movingminus[x] | moving[x] | movingplus[x]) && !scenechange)
                        dst[x] = src[x];
                    else
                    {
                        /* Blend fields. */
                        p0 = src[x];
                        p0 &= 0x00fefefe;

                        p1 = srcminus[x];
                        p1 &= 0x00fcfcfc;

                        p2 = srcplus[x];
                        p2 &= 0x00fcfcfc;

                        dst[x] = (p0>>1) + (p1>>2) + (p2>>2);
                    }
                } while(++x < w);
            }
            else
            {
                // Doing line interpolate. Thus, even lines are going through
                // for moving and non-moving mode. Odd line pixels will be subject
                // to the motion test.
                if (y&1)
                {
                    x = 0;
                    do {
                        if (!(movingminus[x] | moving[x] | movingplus[x]) && !scenechange)
                            dst[x] = src[x];
                        else
                        {
                            if (cubic && (y > 2) && (y < hminus3))
                            {
                                rpp = (srcminusminus[x] >> 16) & 0xff;
                                rp =  (srcminus[x] >> 16) & 0xff;
                                rn =  (srcplus[x] >> 16) & 0xff;
                                rnn = (srcplusplus[x] >>16) & 0xff;
                                gpp = (srcminusminus[x] >> 8) & 0xff;
                                gp =  (srcminus[x] >> 8) & 0xff;
                                gn =  (srcplus[x] >>8) & 0xff;
                                gnn = (srcplusplus[x] >> 8) & 0xff;
                                bpp = (srcminusminus[x]) & 0xff;
                                bp =  (srcminus[x]) & 0xff;
                                bn =  (srcplus[x]) & 0xff;
                                bnn = (srcplusplus[x]) & 0xff;
                                R = (5 * (rp + rn) - (rpp + rnn)) >> 3;
                                if (R > 255) R = 255;
                                else if (R < 0) R = 0;
                                G = (5 * (gp + gn) - (gpp + gnn)) >> 3;
                                if (G > 255) G = 255;
                                else if (G < 0) G = 0;
                                B = (5 * (bp + bn) - (bpp + bnn)) >> 3;
                                if (B > 255) B = 255;
                                else if (B < 0) B = 0;
                                dst[x] = (R << 16) | (G << 8) | B;
                            }
                            else
                            {
                                p1 = srcminus[x];
                                p1 &= 0x00fefefe;

                                p2 = srcplus[x];
                                p2 &= 0x00fefefe;

                                dst[x] = (p1>>1) + (p2>>1);
                            }
                        }
                    } while(++x < w);
                }
                else
                {
                    // Even line; pass it through.
                    memcpy(dst, src, wtimes4);
                }
            }
        }
        src = (Pixel *)((char *)src + srcpitch);
        srcminus = (Pixel *)((char *)srcminus + srcpitch);
        srcplus = (Pixel *)((char *)srcplus + srcpitch);
        if (cubic)
        {
            srcminusminus = (Pixel *)((char *)srcminusminus + srcpitch);
            srcplusplus = (Pixel *)((char *)srcplusplus + srcpitch);
        }
        dst = (Pixel *)((char *)dst + dstpitch);
        moving += w;
        movingminus += w;
        movingplus += w;
    }
    
    // The last line gets a free ride.
    memcpy(dst, src, wtimes4);

filter_done:
    first_frame = 0;
}
