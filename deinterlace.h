typedef unsigned int    Pixel;
typedef unsigned int    Pixel32;
typedef unsigned char    Pixel8;
typedef int        PixCoord;
typedef int        PixDim;
typedef int        PixOffset;

void bgr2argb(char *in, unsigned int *out, int width, int height);
void argb2bgr(unsigned int *in, char *out, int width, int height);

class Deinterlacer {
    unsigned int   width, height;
    int            first_frame;
    int            *prevFrame;
    int            *saveFrame;
    unsigned char  *moving;
    unsigned char  *fmoving;
    
    static const int motionOnly     = 0;
    static const int Blend          = 0;
    static const int threshold      = 15;
    static const int scenethreshold = 100;
    static const int fieldShift     = 0;
    static const int inswap         = 0;
    static const int outswap        = 0;
    static const int highq          = 0;
    static const int diffmode       = 0;
    static const int colordiff      = 1;
    static const int noMotion       = 0;
    static const int cubic          = 0;

public:
    Pixel32        *convertFrameIn;
    Pixel32        *convertFrameOut;
    
    Deinterlacer(int x, int y);
//    ~Deinterlacer();
    void deinterlace();
};
