#!/usr/bin/perl

    $file          = '/var/db/webcam/laima.jpg';
    $flag_file     = '/var/db/webcam/client/present';
    $max_time_c    = 1000;
    $max_no_update = 30;
    $content_type  = 'jpeg';
    $bnd           = '--myboundary';

    # unbuffer, no <> separator
    $| = 1;
    undef $/;

    #print "HTTP/1.0 200 OK\n";
    print "Content-type: multipart/x-mixed-replace; boundary=$bnd\n\n";
    print "$bnd\n";

    $r = $ENV{HTTP_REFERER};
    exit if $r !~ m{http://[^/]+\.fundamental\.lv/} && $r ne '';

    my $started = time();
    alarm(2*$max_time_c);

    while(1)
    {
	$mtime = (stat($file))[9];
	$now = time();

        exit if $now - $started > $max_time_c;

	if($mtime == $mtime_prev) {
	    exit if(($mtime + $max_no_update) < $now);
	} else {
	    $mtime_prev = $mtime;
	    utime($now, $now, $flag_file);

	    print "Content-type: image/$content_type\n\n";
	    open(PIC, "< $file");
	    print STDOUT <PIC>;
	    close(PIC);
	    print "\n\n$bnd\n";
	}
	select(undef, undef, undef, 1);
	#sleep(1);
    }
