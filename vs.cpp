#include <ace/OS.h>
#include <ace/Thread.h>
#include <ace/Thread_Mutex.h>
#include <ace/Condition_T.h>
#include <ace/Log_Msg.h>

#include <Magick++.h>

#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <string>
#include <vector>
#include <iostream>

#include <dev/bktr/ioctl_bt848.h>
#include <dev/bktr/ioctl_meteor.h>

#include "deinterlace.h"

using namespace std;

// device, 0,1,2,3 will be appended in main, ie. /dev/bktr0
string dev("/dev/bktr");
// sleep after all pictures are created, then proceed to next run
ACE_Time_Value sleepBetweenCycles;
// input connectors are wired in this order
const int videoPortMap[] = { METEOR_INPUT_DEV1, METEOR_INPUT_DEV0, METEOR_INPUT_DEV2, METEOR_INPUT_DEV3 };
// one instance of image handling class
Magick::Image img;
Magick::Color *black;
// grab these pictures
class VideoParam;
vector<VideoParam*> vP;
// do not grab more frames until encoder is free
volatile int encoderBusy = 0;
// current parameters for encoding
volatile VideoParam *encoderParams;
// sync between grabber() and encoder()
ACE_Thread_Mutex lck;
ACE_Thread_Condition<ACE_Thread_Mutex> grabberCond(lck);
ACE_Thread_Condition<ACE_Thread_Mutex> encoderCond(lck);

class VideoParam
{
public:
    static const int maxWidth = 768;
    static const int maxHeight = 576;
    static const int maxBrightness = 200;
    static const int maxContrast = 200;
    static const int maxChroma = 200;
    int port;
    int captureX, captureY;
    int cropX, cropY;
    int cropAtX, cropAtY;
    Magick::Geometry *crop;
    int scaleX, scaleY;
    Magick::Geometry *scale;
    int deinterlace;
    Deinterlacer *deinterlacer;
    int autobright;
    int autoleft;
    int brightness;
    int contrast;
    int hue;
    int saturation;
    int textX, textY;
    Magick::Color *textColor;
    Magick::DrawableText *text;
    Magick::DrawableText *text2;
    int jpegQuality;
    char *file;
    char *tempFile;
    Magick::Blob *blob;
    
    VideoParam(char *initStr);
};

VideoParam::VideoParam(char *initStr)
{    
    char delim[] = ":";
    char *t = NULL;
    vector<char*> tokens;
    t = ACE_OS::strtok(initStr, delim);
    while(t != NULL) {
        tokens.push_back(t);
        t = ACE_OS::strtok(NULL, delim);
    }
    ACE_ASSERT(tokens.size() == 21);
    port        = atoi(tokens.at(0));
    captureX    = atoi(tokens.at(1));
    captureY    = atoi(tokens.at(2));
    cropX       = atoi(tokens.at(3));
    cropY       = atoi(tokens.at(4));
    cropAtX     = atoi(tokens.at(5));   
    cropAtY     = atoi(tokens.at(6));   
    scaleX      = atoi(tokens.at(7));   
    scaleY      = atoi(tokens.at(8));   
    deinterlace = atoi(tokens.at(9));   
    autobright  = atoi(tokens.at(10));   
    brightness  = atoi(tokens.at(11));   
    contrast    = atoi(tokens.at(12));   
    hue         = atoi(tokens.at(13));   
    saturation  = atoi(tokens.at(14));   
    textX       = atoi(tokens.at(15));
    textY       = atoi(tokens.at(16));
    char *textColorStr = tokens.at(17); 
    char *textStr      = tokens.at(18); 
    jpegQuality = atoi(tokens.at(19));
    file        = tokens.at(20); 
                                    
    ACE_ASSERT(port >= 0 && port <= 3);
    ACE_ASSERT(captureX >= 100 && captureX <= maxWidth);
    ACE_ASSERT(captureY >= 100 && captureY <= maxHeight);
    ACE_ASSERT(cropX == -1 || (cropX >= 100 && cropX <= captureX));
    ACE_ASSERT(cropY == -1 || (cropY >= 100 && cropY <= captureY));
    ACE_ASSERT(cropAtX == -1 || (cropAtX >= 0 && cropAtX <= captureX));
    ACE_ASSERT(cropAtY == -1 || (cropAtY >= 0 && cropAtY <= captureY));
    ACE_ASSERT(scaleX == -1 || (scaleX >= 100 && scaleX <= captureX));
    ACE_ASSERT(scaleY == -1 || (scaleY >= 100 && scaleY <= captureY));
    ACE_ASSERT(deinterlace == 0 || deinterlace == 1);
    ACE_ASSERT(autobright == -1 || (autobright >= 1 && autobright <= 100));
    ACE_ASSERT(brightness > 0 && brightness <= 300);
    ACE_ASSERT(contrast >= 0 && contrast <= 300);
    ACE_ASSERT(hue >= 0 && hue <= 300);
    ACE_ASSERT(saturation >= 0 && saturation <= 300);
    ACE_ASSERT(textX == -1 || (textX >= 0 && textX <= captureX));
    ACE_ASSERT(textY == -1 || (textY >= 0 && textY <= captureY));
    if(textX != -1 && textY != -1) {
        textColor = new Magick::Color(textColorStr);
        text = new Magick::DrawableText(textX, textY, textStr);
        text2 = new Magick::DrawableText(textX+1, textY+1, textStr);
    } else {
        text = 0;
    }
    ACE_ASSERT(jpegQuality >= 10 && jpegQuality <= 100);
    
    if(cropX != -1 && cropY != -1 && cropAtX != -1 && cropAtY != -1)
        crop = new Magick::Geometry(cropX, cropY, cropAtX, cropAtY);
    else
        crop = 0;
    if(scaleX != -1 && scaleY != -1)
        scale = new Magick::Geometry(scaleX, scaleY);
    else
        scale = 0;
    if(autobright == -1)
        autobright = 0;
    else
        autoleft = autobright - 1;
    // prepend . to file name - dir/file.jpg -> dir/.file.jpg
    string f(file);
    unsigned int slash = f.rfind("/");
    if(slash != string::npos)
        f.insert(slash + 1, ".");
    else    
        f.insert(0, ".");
    tempFile = ACE_OS::strdup(f.c_str());
    if(deinterlace)
        deinterlacer = new Deinterlacer(captureX, captureY);
    blob = 0;
}

void* grabber(void *notused);
void* listener(void *notused);
void* encoder(void *notused);
int calcPicMean(int x, int y, unsigned char *buf);
int calcPicStddev(int x, int y, unsigned char *buf, int picmean);

int main(int argc, char **argv)
{
    ACE_ASSERT(argc >= 4);
    int devI = atoi(argv[1]);
    ACE_ASSERT(devI >= 0 && devI <= 3);
    dev += argv[1];
    sleepBetweenCycles.set(atof(argv[2])); 
    for(int i = 3; i < argc; ++i)
        vP.push_back(new VideoParam(argv[i]));
    black = new Magick::Color("black");
    ACE_Thread::spawn(grabber);
    ACE_Thread::spawn(listener);
    encoder(0);
/* something wrong with ImageMagick DrawableText() and thread stack
    ACE_Thread::spawn(encoder);
    ACE_Thread::spawn(encoder, 0, THR_NEW_LWP|THR_JOINABLE, 0, 0, ACE_DEFAULT_THREAD_PRIORITY, 0, 1024*1024);    
    while(1) ACE_OS::sleep(10000000);
*/  
    // not reached  
    return 0;
}

void* grabber(void *notused __attribute__ ((unused)))
{
    // open and mmap video device
    ACE_HANDLE videoDev;
    ACE_ASSERT((videoDev = ACE_OS::open(dev.c_str(), O_RDONLY)) != -1);
    void *videoBuf = ACE_OS::mmap(0, VideoParam::maxWidth * VideoParam::maxHeight * 3, PROT_READ, MAP_SHARED, videoDev);
    // set video signal format
    int format = BT848_IFORM_F_PALBDGHI;
    ACE_ASSERT(ACE_OS::ioctl(videoDev, BT848SFMT, &format) != -1);
    // set pixel format
    int pixfmtInitialized = 0;
    for(int format = 0;; ++format) {
        meteor_pixfmt pixfmt;
        pixfmt.index = format;
        ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORGSUPPIXFMT, &pixfmt) != -1 || errno == EINVAL);
        if(errno == EINVAL) break;
        if(pixfmt.type == METEOR_PIXTYPE_RGB &&	pixfmt.Bpp == 3) {
            ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORSACTPIXFMT, &format) != -1);
            pixfmtInitialized = 1;
            break;
        }
    }
    ACE_ASSERT(pixfmtInitialized);
    
    int prevPort = -1;
    int prevX = -1, prevY = -1;
    int prevBrightness = -1;
    int prevContrast = -1;
    int prevHue = -1;
    int prevSaturation = -1;
    while(1) {
        vector<VideoParam*>::iterator i;
        for(i = vP.begin(); i != vP.end(); ++i) {
            VideoParam *p = *i;
            // set input
            if(p->port != prevPort) {        
                int src = videoPortMap[p->port];
                ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORSINPUT, &src) != -1);
                prevPort = p->port;
            }
            // set geometry    
            if(p->captureX != prevX || p->captureY != prevY) {
                meteor_geomet geo;
                geo.frames = 1;
                geo.columns = p->captureX;
                geo.rows = p->captureY;
                if(p->captureY <= 240)
                    geo.oformat = METEOR_GEO_RGB24 | METEOR_GEO_ODD_ONLY;
                else
                    geo.oformat = METEOR_GEO_RGB24;
                ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORSETGEO, &geo) != -1);
                prevX = p->captureX;
                prevY = p->captureY;
            }
            // set brightness
            if(p->brightness != prevBrightness) {
                int brightness = p->brightness;
                ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORSBRIG, &brightness) != -1);
                prevBrightness = p->brightness;
            }
            // set contrast
            if(p->contrast != prevContrast) {
                int contrast = p->contrast;
                ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORSCONT, &contrast) != -1);
                prevContrast = p->contrast;
            }
            // set hue
            if(p->hue != prevHue) {
                int hue = p->hue;
                ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORSHUE, &hue) != -1);
                prevHue = p->hue;
            }
            // set chroma saturation
            if(p->saturation != prevSaturation) {
                int saturation = p->saturation;
                ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORSCSAT, &saturation) != -1);
                prevSaturation = p->saturation;
            }
            // capture
            int capmethod = METEOR_CAP_SINGLE;
            ACE_ASSERT(ACE_OS::ioctl(videoDev, METEORCAPTUR, &capmethod) != -1);
            // recalculate brightness and contrast when requested every 'autobright' frames
            if(p->autobright) {
                if(p->autoleft == 0) {
                    int mean = calcPicMean  (p->captureX, p->captureY, static_cast<unsigned char*>(videoBuf));
                    int dev  = calcPicStddev(p->captureX, p->captureY, static_cast<unsigned char*>(videoBuf), mean);
                    if(mean < 256/2 - 10) ++p->brightness;
                    if(mean > 256/2 + 10) --p->brightness;
                    if(dev < 256/6 - 3) ++p->contrast;
                    if(dev > 256/6 + 3) --p->contrast;
                    p->autoleft = p->autobright;
                } else {
                    --p->autoleft;
                }
            }
            // wake up encoder
            lck.acquire();
            while(encoderBusy == 1)
                grabberCond.wait(lck);
            if(p->deinterlace)
                bgr2argb(static_cast<char*>(videoBuf), p->deinterlacer->convertFrameIn, p->captureX, p->captureY);
            else
                img.read(p->captureX, p->captureY, "BGR", Magick::CharPixel, videoBuf);
            encoderParams = p;
            encoderBusy = 1;
            encoderCond.signal();
            lck.release();
        }
        ACE_OS::sleep(sleepBetweenCycles);
    }    
    // not reached
    return NULL;                
}

void* encoder(void *notused __attribute__ ((unused)))
{
    int frames = 0;
    char *tmpBuf = new char[VideoParam::maxWidth * VideoParam::maxHeight * 3];
    while(1) {
        lck.acquire();
        while(encoderBusy == 0)
            encoderCond.wait(lck);
        lck.release();
        volatile VideoParam *p = encoderParams;
        if(p->deinterlace) {
            p->deinterlacer->deinterlace();
            // can't create images directly from BGRA - ImageMagick problem - font is distorted when drawing text
            //img.read(p->captureX, p->captureY, "BGRA", Magick::CharPixel, p->deinterlacer->convertFrameOut);
            argb2bgr(p->deinterlacer->convertFrameOut, static_cast<char*>(tmpBuf), p->captureX, p->captureY);
            img.read(p->captureX, p->captureY, "BGR", Magick::CharPixel, tmpBuf);
        }
        if(p->crop)
            img.crop(*(p->crop));
        if(p->scale)
            img.scale(*(p->scale)); // sample() is much faster then scale()
        if(p->text) {
            img.fontPointsize(13);
            // default is helvetica type1
            //img.font("verdana");
            img.fillColor(*black);
            img.draw(*(p->text2));
            img.fillColor(*(p->textColor));
            img.draw(*(p->text));
        }
        img.quality(p->jpegQuality);
        if(p->blob) {
            img.write(p->blob, "JPG");
            // wake up listener
        } else {
            img.write(p->tempFile);
            ACE_ASSERT(ACE_OS::rename(p->tempFile, p->file) != -1);
        }
        std::cout << frames++ << std::endl;
        lck.acquire();
        encoderBusy = 0;
        lck.release();                
        grabberCond.signal();
    }
    // not reached
    return NULL;                
}

void* listener(void *notused __attribute__ ((unused)))
{
    // not reached
    return NULL;                
}

int calcPicMean(int x, int y, unsigned char *buf)
{
    double rtotal = 0, gtotal = 0, btotal = 0;
    for(int r = 0; r < y; ++r) {
        unsigned char *cp = buf + r*x*3;
        for(int c = 0; c < x; ++c) {
            // bktr buffer is in BGR format
            btotal += *cp++;
            gtotal += *cp++;
            rtotal += *cp++;
        }
    }
    int area = x*y;
    int rmean = (int)(rtotal / area);
    int gmean = (int)(gtotal / area);
    int bmean = (int)(btotal / area);
    return (int)((double)rmean * .299 +
                 (double)gmean * .587 +
                 (double)bmean * .114);
}

int calcPicStddev(int x, int y, unsigned char *buf, int picmean)
{
  int bwtotal = 0;
  for(unsigned char *cp = buf; cp < buf + x*y*3; cp += 3)
        // bktr buffer is in BGR format
        bwtotal += (int)pow((double) *(cp + 2) * .299 +
                            (double) *(cp + 1) * .587 +
                            (double) *(cp + 0) * .114 - picmean, 2);
  return (int)sqrt((double)bwtotal/(x*y));
}
