#!/usr/bin/perl

    $file          = '/var/www/cam0.jpg';
    $max_time_c    = 1000;
    $max_no_update = 40;
    $content_type  = 'jpeg';
    $bnd           = '--myboundary';

    # unbuffer, no <> separator
    $| = 1;
    undef $/;

    # SOL_SOCKET, SO_SNDBUF; Linux is 1, 7
    setsockopt(STDOUT, 0xffff, 0x1001, pack("l", 8192));

    #print "HTTP/1.0 200 OK\n";
    print "Content-type: multipart/x-mixed-replace; boundary=$bnd\n\n";
    print "$bnd\n";

    my $started = time();
    alarm(2*$max_time_c);

    while(1)
    {
        ($mtime, $size) = (stat($file))[9,7];
        $now = time();

        exit if $now - $started > $max_time_c;

        if($mtime == $mtime_prev && $size == $size_prev) {
            exit if(($mtime + $max_no_update) < $now);
        } else {
            $mtime_prev = $mtime;
            $size_prev = $size;

            print "Content-type: image/$content_type\n\n";
            open(PIC, "< $file");
            print STDOUT <PIC>;
            close(PIC);
            print "\n\n$bnd\n";
        }
        select(undef, undef, undef, 0.1);
        #sleep(1);
    }
